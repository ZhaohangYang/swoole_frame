<?php

namespace App\Http\Controller;

use App\Http\Controller\BasicController;
use App\ServiceProvider\Standard\LogService\LogBasic;

class IndexController extends BasicController
{
    public function test()
    {
        LogBasic::debug( 'test' );
    }
}