<?php
namespace App\ServiceProvider\Standard;

use App\ServiceProvider\BasicServiceProvider;
use App\ServiceProvider\Standard\CacheService\CacheBasic;

/**
 * 缓存基础服务
 */
class CacheServiceProvider extends BasicServiceProvider
{

    public function register()
    {
        $serviceConfig = $this->container->get( 'config' );

        CacheBasic::enableFileCache();

        $redis_config = $serviceConfig->get( 'database.redis' );
        if ( $redis_config['enable'] ) {

            CacheBasic::enableRedisCache( $redis_config );
        }
    }

}