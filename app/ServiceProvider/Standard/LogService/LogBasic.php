<?php

namespace App\ServiceProvider\Standard\LogService;

use App\Application;

class LogBasic
{
    /**
     * 错误级别
     */
    const LOGGER_LEVEL_TRACE = 1;
    const LOGGER_LEVEL_DEBUG = 2;
    const LOGGER_LEVEL_WARNING = 3;
    const LOGGER_LEVEL_ERROR = 4;
    const LOGGER_LEVEL_FATAL = 5;

    private static $logPath;

    public static function enableLog()
    {
        self::$logPath = self::getLogPath();
    }

    public static function getLogPath()
    {
        $storage_path = Application::StoragePath();
        $log_path     = $storage_path . DIRECTORY_SEPARATOR . 'logs';

        !is_dir( $log_path ) && mkdir( $log_path );

        return $log_path;
    }

    public static function getLogFile()
    {
        $log_path = self::$logPath ?: self::getLogPath();
        $log_file = $log_path . DIRECTORY_SEPARATOR . date( "Y-m-d" ) . 'log';

        !is_file( $log_file ) && touch( $log_file );

        return $log_file;
    }
    public static function trace($message)
    {
        return self::log( $message, self::LOGGER_LEVEL_TRACE );
    }
    public static function debug($message)
    {
        self::log( $message, self::LOGGER_LEVEL_DEBUG );
    }

    public static function warning($message)
    {
        return self::log( $message, self::LOGGER_LEVEL_WARNING );
    }

    public static function fatal($message)
    {

        return self::log( $message, self::LOGGER_LEVEL_FATAL );
    }

    public static function error($message)
    {
        return self::log( $message, self::LOGGER_LEVEL_ERROR );
    }


    public static function log($message, $level = self::LOGGER_LEVEL_DEBUG)
    {
        $level   = self::formatLevel( $level );
        $message = self::formatMessage( $message );

        $file_name    = self::getLogFile();
        $file_content = date( 'Y-m-d H:i:s' ) . '>> ' . $level . ': ' . $message . PHP_EOL;

        file_put_contents( $file_name, $file_content, FILE_APPEND );
    }


    public static function formatMessage($message)
    {
        if ( $message instanceof \Exception ) {
            $message = $message->getMessage();
        }
        if ( is_array( $message ) ) {
            $message = json_encode( $message, JSON_UNESCAPED_UNICODE );
        }

        return $message;
    }

    public static function formatLevel($level)
    {
        switch ($level) {
            case self::LOGGER_LEVEL_TRACE:
                $level_name = 'TRACE';
                break;
            case self::LOGGER_LEVEL_DEBUG:
                $level_name = 'DEBUG';
                break;
            case self::LOGGER_LEVEL_WARNING:
                $level_name = 'WARNING';
                break;
            case self::LOGGER_LEVEL_ERROR:
                $level_name = 'ERROR';
                break;
            case self::LOGGER_LEVEL_FATAL:
                $level_name = 'FATAL';
                break;
            default:
                $level_name = 'N/A';
        }

        return $level_name;
    }

}