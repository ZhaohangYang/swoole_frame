<?php
namespace App\ServiceProvider\Standard;

use App\ServiceProvider\BasicServiceProvider;
use App\ServiceProvider\Standard\LogService\LogBasic;

/**
 * 标准日志基础服务
 */
class CacheServiceProvider extends BasicServiceProvider
{
    public function register()
    {
        LogBasic::enableLog();
    }

}